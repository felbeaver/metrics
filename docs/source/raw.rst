Модуль **raw**
==============

.. automodule:: metrics.raw

.. autoclass:: metrics.raw.RawMetrics

.. autoclass:: metrics.raw.PhysicalLineCounter
   :members:

.. autofunction:: metrics.raw._count_comments

.. autofunction:: metrics.raw._is_logical

.. autofunction:: metrics.raw.analyze_code

.. autofunction:: metrics.raw.analyze_file

.. autofunction:: metrics.raw.analyze_files

.. autofunction:: metrics.raw.analyze_functions
