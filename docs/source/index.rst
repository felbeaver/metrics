.. metrics documentation master file, created by
   sphinx-quickstart on Tue Feb  9 02:01:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в документацию пакета Metrics!
===============================================

Metrics - это пакет Python для вычисления различных метрик программного обеспечения, написанного на языке C. Этот пакет может вычислять:

   * LOC метрики (LOC, LLOC, PLOC, пустые строки, разные средние значения строк и проценты строк и т. д.);
   * метрики Холстеда;
   * метрика Мак-Кейба (цикломатическая сложность);
   * метрики информационного потока Генри-Кафуры (сложность модуля).

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   raw
   halstead
   filters
   parser

Указатели и таблицы
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
