Модуль **halstead**
===================

.. automodule:: metrics.halstead

.. autoclass:: metrics.halstead.BasicHalsteadMetrics

.. autofunction:: metrics.halstead._is_operand

.. autofunction:: metrics.halstead._get_operand_spelling

.. autofunction:: metrics.halstead._is_operator

.. autoclass:: metrics.halstead.IfKind

.. autofunction:: metrics.halstead._get_operator_spelling

.. autofunction:: metrics.halstead._add_operators_from_tokens

.. autofunction:: metrics.halstead.analyze_file

.. autofunction:: metrics.halstead.analyze_files
